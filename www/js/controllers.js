angular.module("app.controllers", ['google.places', 'ui.mask'])
    .controller('formularioCtrl', function ($scope, $timeout) {

        $scope.livesPlace = null;
        $scope.teste = {};
        $scope.objetoCadastro = {
            type: "post_data",
            case: "users.register",
            data: {}
        };

        $scope.termos = false;


        $scope.autocompleteOptions = {
            componentRestrictions: { country: 'br' },
            types: ['(cities)'],
            language: 'pt-br'
        };

        var conn;
        var onConnectCallback = function () {
                $subs = conn.subscribe('/temp-queue/me', onMessageCallback);
            },
            onErrorCallback = function (error) {
                console.log('STOMP: ' + error );
                $timeout(stompConnect, 2000);
                console.log('STOMP: Reconecting in 2 seconds');
            },
            onMessageCallback = function( message )
            {
                if( message.body )
                {
                    alert(message.body);
                }
            };
        
        function stompConnect() {
            console.log('STOMP: Attempting connection');
            conn = Stomp.client( 'ws://yuudy2dev.skruuble.com:61623' );
            conn.heartbeat.outgoing = 0;
            conn.heartbeat.incoming = 0;
            conn.connect('test29', 'v1b2n3m4', onConnectCallback, onErrorCallback);
            
        }

        stompConnect();

        $scope.capturePhoto = function () {

            navigator.camera.getPicture($scope.getFileEntry, $scope.onFail, {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                cameraDirection: 0,
                saveToPhotoAlbum: true,
                correctOrientation: true
            });
        };

        $scope.getPhoto = function () {
            navigator.camera.getPicture($scope.getFileEntry, $scope.onFail, {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                correctOrientation: true

            });
        };

        $scope.getFileEntry = function (imgUri) {

            var profilePicture = document.getElementById('profile_picture');
            profilePicture.style.backgroundImage =  'url(data:image/jpeg;base64,' + imgUri + ')';
            profilePicture.style.backgroundSize =  '100px 100%';

            $scope.image64 = 'data:image/jpeg;base64,' + imgUri;

        };

        $scope.onFail = function () {

        };

        $scope.cadastrarUsuario = function (image64, objetoCadastro, liveSplace) {

            console.log(objetoCadastro);

            AWS.config.accessKeyId = 'AKIAJNNL3AUVNMJ4H55A';
            AWS.config.secretAccessKey = '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';
            AWS.config.region = 'sa-east-1';
            var s3 = new AWS.S3({params: {Bucket: 'yuudy-photos'}});
            console.log('Depois da var S3');

            function data(ref){
                if(ref == "ano") {
                    return res = new Date().getFullYear();
                }
                if(ref == "dia"){
                    preRes = new Date().getDate();
                    if(preRes.toString().length == 1){
                        return res = "0" + preRes;
                    }else{
                        return preRes;
                    }
                }if(ref == "mes") {
                    preRes = new Date().getMonth() + 1;
                    if (preRes.toString().length == 1){
                        console.log(preRes);
                        return res = "0" + preRes;
                    }else{
                        return preRes;
                    }
                }
            }

            albumName =  data('ano');

            if (!albumName) {
                return alert('O album não contém nenhum carácter');
            }

            var albumKey = encodeURIComponent(albumName) + '/';
            s3.headObject({Key: albumKey}, function(err, data) {
                if (!err) {

                }
                if (err.code !== 'NotFound') {
                    $ionicLoading.hide();
                    return alert('Houve um erro na criação do seu album: ' + err.message);
                }
                s3.putObject({Key: albumKey}, function(err, data) {
                    if (err) {
                        $ionicLoading.hide();
                        return alert('Houve um erro na criação do seu album: ' + err.message);
                    }


                });
            });

            var names = function () {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();

            };


            var file = image64;
            var fileName = names();
            var albumPhotosKey = encodeURIComponent(albumName) + '/' + data('mes') + '/' + data('dia') + '/';

            photoKey = albumPhotosKey + fileName;
            s3.upload({
                Key: photoKey,
                Body: file,
                ContentEncoding: 'base64',
                ContentType: 'image/jpeg',
                ACL: 'public-read'
            }, function (err, data) {
                if (err) {
                    return alert('There was an error uploading your photo: ', err.message);
                }

                objetoCadastro.data.profile_picture = "https://s3.amazonaws.com/yuudy-photos/" + photoKey;
                objetoCadastro.data.livesplace = liveSplace.address_components[0].long_name + ' - ' + liveSplace.address_components[2].short_name;

                var tx = conn.begin();
                conn.send('/queue/server', {"reply-to": '/temp-queue/me'}, JSON.stringify(objetoCadastro));
                tx.commit();

            });
        }

    });