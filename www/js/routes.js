angular.module('app.routes', ['ui.router'])

    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state('formulario', {
                url: '/page1',
                templateUrl: 'templates/formulario.html',
                controller: 'formularioCtrl'

            });


        $urlRouterProvider.otherwise('/page1')



    });
